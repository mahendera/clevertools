package com.clevertools.tools;

import com.clevertools.tools.service.JSONToolsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JSONToolsTest {

    @Autowired
    private JSONToolsService jsonToolsService;
    private String jsonData = """
    {"name":"Mahender","age":12, "social":{"google":"mahendera@gmail.com","facebook":"mahednera@faceboo"}}
""";

    @Test
    void testJsonFormat() throws JsonProcessingException {
        System.out.println(jsonToolsService.formatJson(jsonData,2));
    }
}
