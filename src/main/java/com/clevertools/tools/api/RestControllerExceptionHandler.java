package com.clevertools.tools.api;

import com.clevertools.tools.dto.ApiResponse;
import com.clevertools.tools.exceptions.ApplicationException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Arrays;

@RestControllerAdvice
@Slf4j
public class RestControllerExceptionHandler {




    @ExceptionHandler(value = {ApplicationException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @Operation(summary = "Returns error response with success as false")
    @ApiResponses({
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500",
                    content = @Content(examples = {
                            @ExampleObject(value="{\"success\":false,\"messageCodes\":[\"unknown.error\"]}" )
                    }, mediaType = MediaType.APPLICATION_JSON_VALUE),
                    description = "returns error response(success=false,body=null) with messageCodes")
    })
    public ApiResponse<Void> applicationException(ApplicationException exception, WebRequest request){
        log.error("error while processing request", exception);
        return ApiResponse.<Void>builder().success(false).messageCodes(exception.getErrorCodes()).build();
    }

    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @Operation(summary = "Returns error response with success as false")
    @ApiResponses({
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500",
                    content = @Content(examples = {
                            @ExampleObject(value="{\"success\":false,\"messageCodes\":[\"unknown.error\"]}" )
                    }, mediaType = MediaType.APPLICATION_JSON_VALUE),
                    description = "returns error response(success=false,body=null) with messageCodes")
    })
    public ApiResponse<Void> exception(Throwable exception, WebRequest request){
        log.error("unhandled exception ",exception);
        return ApiResponse.<Void>builder().success(false).messageCodes(Arrays.asList("unknown.error")).build();
    }


}
