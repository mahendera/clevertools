package com.clevertools.tools.api;

import com.clevertools.tools.dto.ApiResponse;
import com.clevertools.tools.service.JSONToolsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("json")
public class JSONToolsApi {


    @Autowired
    private JSONToolsService jsonToolsService;

    @PostMapping("format/{indentation}")
    public ApiResponse<String> formatJson(@RequestBody String jsonData, @PathVariable("indentation") int indentation) throws JsonProcessingException {
        return ApiResponse.<String>builder().body(jsonToolsService.formatJson(jsonData, indentation)).success(true).build();
    }


}
