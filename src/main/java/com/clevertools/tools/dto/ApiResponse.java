package com.clevertools.tools.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ApiResponse<T> {

    private boolean success;
    private List<String> messageCodes;
    private T body;
}
