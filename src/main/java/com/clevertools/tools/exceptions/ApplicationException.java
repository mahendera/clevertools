package com.clevertools.tools.exceptions;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@ToString
public class ApplicationException extends RuntimeException {

    private List<String> errorCodes;

    public ApplicationException(String errorCode){
        super(errorCode);
        this.errorCodes= Arrays.asList(new String[]{errorCode});
    }

    public ApplicationException(String ...codes){
        this.errorCodes= Arrays.stream(codes).filter(e-> e!=null).toList();
    }

    public ApplicationException(Throwable th, String errorCode){
        super(errorCode, th);
        errorCodes=new ArrayList<>();
        errorCodes.add(errorCode);
    }

}
