package com.clevertools.tools.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface JSONToolsService {
    String formatJson(String jsonData, int indentation);
}
