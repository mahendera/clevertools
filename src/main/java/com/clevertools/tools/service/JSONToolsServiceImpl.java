package com.clevertools.tools.service;

import com.clevertools.tools.exceptions.ApplicationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Service;

@Service
public class JSONToolsServiceImpl implements JSONToolsService {

    @Override
    public String formatJson(String jsonData, int indentation) {
        var objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        try {
            return objectMapper.writeValueAsString(objectMapper.readValue(jsonData, Object.class));
        } catch (JsonProcessingException e) {
            throw new ApplicationException(e.getMessage());
        }
    }
}
